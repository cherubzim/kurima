angular.module('kurima', [
    'mobile-angular-ui',
    'kurima.controllers',
    'kurima.services',
    'ngRoute',
    'ngDialog',
    'angularSpinner'
])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider
    .when("/profile/:id", {
        //used for the first time profile
        templateUrl: "profile.html",
        controller: "profileControl",
    })
    .when("/notifications", {
        templateUrl: "notifications.html",
        controller: "notificationsControl",
    })
    .when("/production", {
        templateUrl: "production.html",
        controller: "productionControl",
    })
    .when("/viewproduct", {
        templateUrl: "viewproduct.html",
        controller: "viewproductControl",
    })
    .when("/addproduct", {
        templateUrl: "addproduct.html",
        controller: "addproductControl",
    })
    .when("/products/:search", {
        templateUrl: "products.html",
        controller: "productsControl",
    })
    .when("/products", {
        templateUrl: "products.html",
        controller: "productsControl",
    })
    .when("/profile", {
        templateUrl: "profile.html",
        controller: "profileControl",
    })
    .when("/supplier", {
        templateUrl: "supplier.html",
        controller: "supplierControl",
    })
    .when("/collections", {
        templateUrl: "collections.html",
        controller: "collectionsControl",
    })
    .when("/categories", {
        templateUrl: "categories.html",
        controller: "catControl",
    })
    .when("/suppliers/:search/:value", {
        templateUrl: "suppliers.html",
        controller: "suppliersControl",
    })
    .when("/suppliers", {
        templateUrl: "suppliers.html",
        controller: "suppliersControl",
    })
    .when("/post", {
        templateUrl: "post.html",
        controller: "postControl",
    })
    .when("/viewPost", {
        templateUrl: "view.html",
        controller: "viewControl",
    })
    .when("/feed/:search/:value", {
        templateUrl: "feed.html",
        controller: "feedControl",
    })
    .when("/feed", {
        templateUrl: "feed.html",
        controller: "feedControl",
    })
    .when("/gettingStarted", {
        templateUrl: "gettingstarted.html",
        controller: "gettingStartedControl",
    })
    .when("/", {
        templateUrl: "landing.html",
        controller: "landingControl",
    })
    .when("/signin", {
        templateUrl: "signin.html",
        controller: "signinControl",
    })
    .when("/signup", {
        templateUrl: "signup.html",
        controller: "signupControl",
    })
    .otherwise({ redirectTo: '/' });

}])

.run([function() {
    //setting the status bar margin and color
    $( document ).on( "deviceready", function(){
        $(".datepicker").datepicker({dateFormat: "D d M yy"});

        if( typeof localStorage.userid != "undefined" ) StatusBar.overlaysWebView( false );
        StatusBar.styleDefault();
        StatusBar.backgroundColorByHexString("#F7F7F7");

        //set different notification bar background for android
        if (cordova.platformId == 'android') {
            StatusBar.backgroundColorByHexString("#333");
        }
    });
}]);
