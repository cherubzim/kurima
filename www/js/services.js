angular.module('kurima.services', [])

.factory('notificationsService', function($http, $location, global, $rootScope){
    var ns = {};

    ns.getNotifications = function($scope){
      global.startSpin();
      $http({
        method: 'JSONP',
        url: "http://"+global.host+"/appserver.php?callback=JSON_CALLBACK&p&action=getNotifications",
        params: {"memberid" : localStorage.userid},
      })
      .then(function(response) {
        log(response);
        var data = response.data;

        if( data.length == 0 )  $scope.nonotifications = true;
        else if( data.length > 0 )  $scope.nonotifications = false;

        $scope.notifications = data;

        global.stopSpin();
      })
    }

    return ns;
})

.factory('supplierService', function($http, $location, global, $rootScope, appService){
    var su = {};

    su.follow = function(rootscope, id, bool, fscope, i, i2){
        global.startSpin();
        if( typeof fscope == undefined ) fscope={};
        $http({
          method: 'JSONP',
          url: "http://"+global.host+"/appserver.php?callback=JSON_CALLBACK&p&action=follow",
          params: {"supplierid" : id, "memberid" : localStorage.userid, "bool" : bool},
        })
        .then(function(response) {
          log(response);
          var data = response.data;

          rootscope.$followers = data.followers;
          if( typeof fscope != "undefined" ) fscope.totalfollowing = data.totalfollowing;

          //log(fscope);

          //false bool means user hasnt followed the supplier. So now we change that value
          //for the supplier list use ids to update buttons

          log( i+" : "+i2 +" : "+id);
          if( bool ) {
            rootscope.$following = false;
            //for the supplier list use ids to update buttons in that specific scope
            //scope has to be sent by the function. However this scope is not sent when the function
            //is called from the single supplier page hence the if statement
            if( typeof fscope != "undefined" && typeof i2 == "undefined" ) {
              //need totalfollowing for the getting started page
              fscope.suppliers[i].following = false;
            }else if( typeof i2 != "undefined" ) fscope.categories[i2].suppliers[i].following = false;

          }else if ( !bool ) {
            rootscope.$following = true;
            //for the supplier list use ids to update buttons in that specific scope
            //scope has to be sent by the function. However this scope is not sent when the function
            //is called from the single supplier page hence the if statement
            if( typeof fscope != "undefined" && typeof i2 == "undefined" ) {
              fscope.suppliers[i].following = true;
            }else if( typeof i2 != "undefined" ) fscope.categories[i2].suppliers[i].following = true;

          }
          global.stopSpin();
        }, function() {
          log(response);
          //global.stopSpin();
          global.stopSpin();
        });
    }

    su.updateProduct = function($scope, id) {
      global.startSpin();
      var form = $("#addproduct").serializeObject();
      log(form);
      $http({
          method: 'JSONP',
          url: "http://"+global.host+"/appserver.php?callback=JSON_CALLBACK&p&action=updateProduct",
          params: form
      })
      .then(function(response) {
          log(response);
          var data = response.data;
          $scope.error = false;

          if( data.ok ) {
            //clear error messages
            $(".help-block").hide();
            $(".has-error").removeClass("has-error");

            localStorage.viewproduct = data.id;
            $location.path("/viewproduct");
          }else {
            log("fail");
            $(".help-block").hide();
            var e = data.message;
            for (var a in e) {
                $(e[a].tag).parents(".form-group").addClass("has-error").find(".help-block").show().html(e[a].message);
            };
          };

          global.stopSpin();
      },
      function(response) {
          log(response);
          $scope.error = true;
          global.stopSpin();
      });
    }

    su.getProduct = function($scope, id) {
      if( id == undefined ) id = 0;
      $scope.url = "img/product.png";

      global.startSpin();
      $http({
          method: 'JSONP',
          url: "http://"+global.host+"/appserver.php?callback=JSON_CALLBACK&p&action=getProduct",
          params: {productid: id}
      })
      .then(function(response) {
          log(response);
          var data = response.data;
          $scope.error = false;

          for( var i in data ) {
            $scope[i] = data[i];
          }
          if( data != false && data.url != undefined ) $scope.url = data.url;
          else delete $scope.url;

          $scope.selfID = localStorage.userid;
          global.stopSpin();
      },
      function(response) {
          log(response);
          $scope.error = true;
          global.stopSpin();
      });
    }

    su.getProducts = function($scope, search) {
      global.startSpin();

      var params = {};
      params.userid = localStorage.userid;
      if( typeof search != 'undefined' ) {
        params.search = search;
        //if its a search then remove add new product buttons
        $scope.forbidnewproduct = true;
      }

      $http({
          method: 'JSONP',
          url: "http://"+global.host+"/appserver.php?callback=JSON_CALLBACK&p&action=getProducts",
          params: params
      })
      .then(function(response) {
          log(response);
          $scope.error = false;//clear error message

          $scope.products = response.data;
          if( $scope.products.length == 0 || $scope.products == false ) $scope.noproducts = true;

          global.stopSpin();
      },
      function(response) {
          log(response);
          $scope.error = true;
          global.stopSpin();
      });
    }

    su.updateProfile = function($scope, b) {
      global.startSpin();
      var form = $("#signupform").serializeObject();
      log(form);
      $http({
          method: 'JSONP',
          url: "http://"+global.host+"/appserver.php?callback=JSON_CALLBACK&p&action=signup",
          params: form
      })
      .then(function(response) {
          log(response);
          var data = response.data;

          if( data.ok ) {
            //clear error messages
            $(".help-block").hide();
            $(".has-error").removeClass("has-error");

              log(b);
              if( b ) {
                  $location.path("/feed");
              }else {
                  $(function () {
                      $scope.updatesuccess = true;
                  })
              }
          }else {
              log("fail");
              $(".help-block").hide();
              var e = data.message;
              for (var a in e) {
                  $(e[a].tag).parents(".form-group").addClass("has-error").find(".help-block").show().html(e[a].message);
              };
          };

          global.stopSpin();
      });
    }

    su.getProfile = function($scope, id) {

        global.startSpin();
        $http({
            method: 'JSONP',
            url: "http://"+global.host+"/appserver.php?callback=JSON_CALLBACK&p&action=getSupplier",
            params: { supplierid: id, memberid: localStorage.userid }
        })
        .then(function(response) {
            log(response);
            var data = response.data;

            $scope.error = false;//clear error message

            //apply to both view profile and edit profile
            for( var i in data ) $scope[i] = data[i];

            $scope.avator = data.avator;
            if( $scope.products.length == 0 || $scope.products == false ) $scope.noproducts = true;

            //add viewsuplier to $rootScope to allow navbars to interact with the value
            $rootScope.$viewsupplier = localStorage.viewsupplier;

            //these variables are being assigned to $rootScope to allow navbars to use them
            if( data.following ) $rootScope.$following = true;
            else if ( !data.following ) $rootScope.$following = false;
            $rootScope.$followers = data.followers;

            //userid is for the profile.html scope, $selfID is for the supplier.html navbar scope
            $scope.userid = $rootScope.$selfID = localStorage.userid;
            global.stopSpin();

        },
        function(response) {
            log(response);
            $scope.error = true;
            global.stopSpin();
        });
    }

    return su;
})

.factory('appService', function($http, $location, global, $rootScope){

    var ac = {};

    ac.getCollections = function($scope){

        global.startSpin();
        $http({
            method: 'JSONP',
            url: "http://"+global.host+"/appserver.php?callback=JSON_CALLBACK&p&action=getCollections",
            params: { memberid: localStorage.userid }
        })
        .then(function(response) {
            log(response);

            $scope.error = false;//clear error message
            $scope.collections = response.data;

            if( response.data.length == 0 && !$scope.error ) $scope.nocollections = true;
            else $scope.nocollections = false;

            global.stopSpin();
        },
        function(response) {
            log(response);
            $scope.error = true;
            global.stopSpin();
        });

    }

    ac.getCategories = function($scope){

        global.startSpin();
        $http({
            method: 'JSONP',
            async:true,
            crossDomain:true,
            url: "http://"+global.host+"/appserver.php?callback=JSON_CALLBACK&p&action=categories",
            params: { "memberid": localStorage.userid }
        })
        .then(function(response) {
            log(response);

            $scope.error = false;//clear error message
            $scope.categories = response.data;

            //need totalfollowing for the getting started page
            $scope.totalfollowing = response.data.totalfollowing;
            delete response.data.totalfollowing;

            global.stopSpin();

        },
        function(response) {
            log(response);
            $scope.error = true;
            global.stopSpin();
        });

    }

    ac.getSuppliers = function($scope, search, value){
        global.startSpin();

        params = {};
        params.memberid = localStorage.userid;
        if( typeof search != 'undefined' ) {
          params.search = search;
          params.value = value;
        }
        $http({
            method: 'JSONP',
            async:true,
            crossDomain:true,
            url: "http://"+global.host+"/appserver.php?callback=JSON_CALLBACK&p&action=getSuppliers",
            params: params
        })
        .then(function(response) {
            log(response);

            $scope.error = false;//clear error message
            for( var a in response.data ) response.data[a].avator = response.data[a].avator;

            //need totalfollowing for the getting started page
            $scope.totalfollowing = response.data.totalfollowing;
            delete response.data.totalfollowing;

            $scope.suppliers = response.data;

            if( $scope.suppliers.length == 0 && !$scope.error ) $scope.nosuppliers = true;

            $scope.selfID = localStorage.userid;

            global.stopSpin();
        },
        function(response) {
            log(response);
            $scope.error = true;
            global.stopSpin();
        });

    }

    return ac;
})

.factory('feedService', function($http, $location, global, supplierService, $rootScope){
    var gf = {};

    gf.getLand = function($scope){
        //log(1);
        $http({
            method: 'JSONP',
            url: "http://"+global.host+"/appserver.php?callback=JSON_CALLBACK&p&action=land",
            params: { "memberid": localStorage.userid },
        })
            .then(function(response) {
                log(response);
                var data = response.data;

                $scope.land = data;

            });
    }

    gf.getLivestock = function($scope){
        //log(1);
        $http({
            method: 'JSONP',
            url: "http://"+global.host+"/appserver.php?callback=JSON_CALLBACK&p&action=livestock",
            params: { "memberid": localStorage.userid },
        })
            .then(function(response) {
                log(response);
                var data = response.data;

                $scope.livestock = data;

            });
    }

    gf.getCrops = function($scope){
        //log(1);
        $http({
            method: 'JSONP',
            url: "http://"+global.host+"/appserver.php?callback=JSON_CALLBACK&p&action=crops",
            params: { "memberid": localStorage.userid },
        })
        .then(function(response) {
            log(response);
            var data = response.data;

            $scope.crops = data;

        });
    }

    gf.getDistricts = function($scope){
        //log(1);
        $http({
            method: 'JSONP',
            url: "http://"+global.host+"/appserver.php?callback=JSON_CALLBACK&p&action=districts",
            params: { "memberid": localStorage.userid },
        })
        .then(function(response) {
            log(response);
            var data = response.data;

            $scope.districts = data;

        });
    }

    gf.collections = function($scope, bool, id){
        $http({
            method: 'JSONP',
            url: "http://"+global.host+"/appserver.php?callback=JSON_CALLBACK&p&action=collections",
            params: {"postid" : id, "memberid" : localStorage.userid, "bool" : bool},
        })
        .then(function(response) {
            log(response);
            var data = response.data;

            $scope.numcollections = data.numcollections;

            //false bool means user hasnt favourited the post. So now we change that value
            if( bool == "true" ) $scope.collected = false;
            else if ( bool == "false" ) $scope.collected = true;

        });
    }

    gf.placeFav = function($scope, bool){
        $http({
            method: 'JSONP',
            url: "http://"+global.host+"/appserver.php?callback=JSON_CALLBACK&p&action=favs",
            params: {"postid" : localStorage.postid, "memberid" : localStorage.userid, "bool" : bool},
        })
        .then(function(response) {
            log(response);
            var data = response.data;

            $scope.numfavs = data.numfavs;

            //false bool means user hasnt favourited the post. So now we change that value
            if( bool == "true" ) $scope.faved = false;
            else if ( bool == "false" ) $scope.faved = true;

        });
    }

    gf.getPost = function($scope, id){

        global.startSpin();
        $http({
            method: 'JSONP',
            url: "http://"+global.host+"/appserver.php?callback=JSON_CALLBACK&p&action=getPost",
            params: { "id": id, "memberid": localStorage.userid }
        })
        .then(function(response) {
            log(response);
            var data = response.data;

            for( var i in data ) $scope[i] = data[i];

            $scope.timeline = data.timeline+" ago";
            $scope.responseto = data.id;
            $scope.avator = data.avator;

            if( data.favs == "1") $scope.faved = true;
            $scope.collected = ( data.collected == "1") ? true : false;

            if(data.url != "") {
                $scope.url = data.url;
                $scope.hasImage = true;
            }else $scope.hasImage = false;

            global.stopSpin();
        });

    }

    gf.placePost = function($scope, bool, fscope, feedService){
        global.startSpin();

        var message = $("#newpostform #message").val();
        var params = { "memberid" : localStorage.userid, "message": message };

        switch( bool ){//do something different depending on whether its a main post or comment
            case true:
                params.category = $("#newpostform #category").val();
                params.responseto = 0;
                break;
            case false:
                params.category = "-";
                params.responseto = $("#responseto").val();
                break;
        }
        log(params);

        $http({
            method: 'JSONP',
            url: "http://"+global.host+"/appserver.php?callback=JSON_CALLBACK&p&action=post",
            params: params,
        })
        .then(function(response) {
            log(response);
            var data = response.data;

            $(".help-block").hide();
            $(".has-error").removeClass("has-error");

      			if(data.ok) {
              //clear error messages
              $(".help-block").hide();
              $(".has-error").removeClass("has-error");

              $("#message").val("");
              //if user uploaded image
              if( bool && $("#displayimage").attr("src") != "" )
                feedService.uploadPhoto(localStorage.imageURI, data.id, fscope, bool, "post");
              //if not
              else {
                if( bool ) $location.path("/feed");//gf.getFeed(fscope);
                else gf.getPost(fscope, localStorage.postid);
              }
              $("#displayimage").hide();
              $("#newpostform #message").val("");
      			}else {
      				log("fail");
      				$(".help-block").hide();
      				var e = data.message;
      				for (var a in e) {
      					$(e[a].tag).parents(".form-group").addClass("has-error").find(".help-block").show().text(e[a].message);
      				};
      			};
            global.stopSpin();

        });
    }

    gf.uploadPhoto = function(imageURI, postid, fscope, bool, type) {

        global.startSpin();

        var options = new FileUploadOptions();
        options.fileKey="file";
        options.fileName=imageURI.substr(imageURI.lastIndexOf('/')+1);
        options.mimeType="image/jpeg";

        var params = new Object();
        params.postid = postid;
        params.memberid = localStorage.userid;
        params.type = type;

        options.params = params;
        options.chunkedMode = false;

        function win(r) {
            console.log("Code = " + r.responseCode);
            console.log("Your post was successful : " + r.response);
            console.log("Sent = " + r.bytesSent);
            //alert("Your post was successful : " + r.response);

            switch(type) {
              case "post" :
                if( bool ) $location.path("/feed");
                else if( !bool ) gf.getPost(fscope, localStorage.postid);
                //else
                break;
              case "avator" :
                supplierService.getProfile(fscope, localStorage.userid);
                break;
              case "cover" :
                supplierService.getProfile(fscope, localStorage.userid);
                break;
            }
        }

        function fail(error) {
            alert("An error has occurred: Code = " + error.code);
        }

        var ft = new FileTransfer();
        ft.upload(imageURI, "http://"+global.host+"/appserver.php", win, fail, options);
    }

    gf.getImage = function(type, $scope) {

        navigator.camera.getPicture(onSuccess, onFail,
        {
            quality: 50,
            destinationType: Camera.DestinationType.FILE_URI,
            sourceType : Camera.PictureSourceType.PHOTOLIBRARY,
            mediaType : Camera.MediaType.ALLMEDIA,
            allowEdit : true,
        });

        function onSuccess(imageURI) {

            switch(type) {
              case "post" :
                var image = document.getElementById('displayimage');
                image.src = localStorage.imageURI = imageURI;
                $("#displayimage").show();
                break;
              case "avator" :
                $("#displayimage").attr("src", imageURI);
                gf.uploadPhoto(imageURI, localStorage.userid, $scope, false, "avator");
                break;
              case "cover" :
                gf.uploadPhoto(imageURI, localStorage.userid, $scope, false, "cover");
                $("#cover").css("background", "url("+ imageURI +")");
                break;
            }
        }

        function onFail(message) {
            //alert('Failed because: ' + message);
        }

    }

    gf.getFeed = function($scope, search, value){
        global.startSpin();

        params = {};
        params.memberid = localStorage.userid;
        if( typeof search != 'undefined' ) params.search = search;
        if( typeof value != 'undefined' ) params.value = value;

        log(params);
        $http({
            method: 'JSONP',
            url: "http://"+global.host+"/appserver.php?callback=JSON_CALLBACK&p&action=getFeed",
            params: params
        })
        .then(function(response) {
            log(response);
            var data = response.data;
            var l = true;

            $scope.error = false;//clear error message

            $scope.leftside = [];
            $scope.rightside = [];

            if( data == false || typeof data == undefined || data.length == 0 && !$scope.error ) $scope.notfollowing = true;
            else $scope.notfollowing = false;//clear following message

            for( var a in data ) {
                var obj = data[a];
//                obj.avator = ( obj.avator == "" ? "img/avator.png" : obj.avator );

                obj.hasurl = obj.url != "" ? true: false;
                obj.faved = obj.favs == "1" ? true: false;
                obj.collected = obj.collected == "1" ? true: false;

                if ( l ) {
                    $scope.leftside.push(obj);
                    l = false;
                }else if( !l ) {
                    $scope.rightside.push(obj);
                    l = true;
                }
                delete obj;
            }

            global.stopSpin();
        },
        function(response) {
            log(response);
            $scope.error = true;
            global.stopSpin();
        })
    };

    gf.getCategories = function($scope){
      global.startSpin();
      $http({
          method: 'JSONP',
          url: "http://"+global.host+"/appserver.php?callback=JSON_CALLBACK&p&action=getCategories",
          params: { "memberid": localStorage.userid },
      })
      .then(function(response) {
          log(response);
          $scope.categories = response.data;
          global.stopSpin();
      });
    }

    return gf;
})

.factory('loginService', function($http, $location, global, $rootScope){
    var lc = {};

    lc.signUp = function() {
        var su = $("#signupform").serializeObject();
        //log(su);
        //clear error messages
        $(".help-block").hide();
        $(".has-error").removeClass("has-error");
        global.startSpin();

        $http({
            method: 'JSONP',
            url: 'http://'+global.host+'/appserver.php?callback=JSON_CALLBACK&p&action=signup',
            params: su,
        })
        .then(function(response) {
            log(response);
            data = response.data;

            if(data.ok) {
              log("success");
              localStorage.userid = data.id;//set user id
              localStorage.supplier = (data.supplier == 1) ? true : false;//set admin
              localStorage.email = data.email;//set email
              $location.path("/gettingStarted");
            }else {
              log("fail");
              $(".help-block").hide();
              var e = data.message;
              for (var a in e) {
                  $(e[a].tag).parents(".form-group").addClass("has-error").find(".help-block").show().html(e[a].message);
              };
            };
            global.stopSpin();
        });
    }

    lc.check = function(email, $scope){
        global.startSpin();
        //clear error messages
        $(".help-block").hide();
        $(".has-error").removeClass("has-error");
        var email = $("#email").val();

        $http({
            method: 'JSONP',
            url: 'http://'+global.host+'/appserver.php?callback=JSON_CALLBACK&p&action=checkUser',
            params: { "email": email },
        })
        .then(function(response) {
            log(response);
            data = response.data;
            if(data.ok) {

              //if its a supplier then they should enter their password.
              if( data.supplier == 1 ) $scope.found = true;
              //normal users get to go in free
              else if ( data.supplier == 0 ) {
                localStorage.userid = data.id;//set user id
                localStorage.supplier = (data.supplier == 1) ? true : false;//set admin
                localStorage.email = data.email;//set email
                $location.path("/feed");
              }

              global.stopSpin();
            }else {
              global.stopSpin();
                log("fail");
                $(".help-block").hide();
                var e = data.message;
                for (var a in e) {
                    $(e[a].tag).parents(".form-group").addClass("has-error").find(".help-block").show().text(e[a].message);
                };
            };
          });
      }

    lc.login = function(email, password){
        global.startSpin();
        //clear error messages
        $(".help-block").hide();
        $(".has-error").removeClass("has-error");
        var email = $("#email").val();

        $http({
            method: 'JSONP',
            url: 'http://'+global.host+'/appserver.php?callback=JSON_CALLBACK&p&action=signin',
            params: { "email": email, "password": password },
        })
        .then(function(response) {
            log(response);
            data = response.data;
            if(data.ok) {

                log("success");
                localStorage.userid = data.id;//set user id
                localStorage.supplier = (data.supplier == 1) ? true : false;//set admin
                localStorage.email = data.email;//set email
                $location.path("/feed");
                //log(localStorage.supplier);

            }else {
                log("fail");
                $(".help-block").hide();
                var e = data.message;
                for (var a in e) {
                    $(e[a].tag).parents(".form-group").addClass("has-error").find(".help-block").show().text(e[a].message);
                };
            };
            global.stopSpin();
        });
    }

    lc.checkLogin = function() {
        if( typeof(localStorage.userid) == "undefined" || typeof(localStorage.userid) == "null" ) return false;
        else if( typeof(localStorage.userid) != "undefined" && typeof(localStorage.userid) != "null" ) return true
    }

    lc.logout = function($scope) {
        delete localStorage.userid;
        delete localStorage.supplier;
        delete localStorage.email;
        $rootScope.$navbartop = false;
        $rootScope.$navbarbottom = false;

        $location.path("/signin");
        try { StatusBar.overlaysWebView( true ); }
        catch (e) { log(e); }
    }

    return lc;
})

.factory('global', function(usSpinnerService, $http, $rootScope){
    return {
        //host : 'thf.esy.es/kurima',
        host : 'localhost/kurima',
        startSpin: function() {
            usSpinnerService.spin('mySpinner');
        },
        stopSpin: function() {
            usSpinnerService.stop('mySpinner');
        },
        updateNavBar: function(back, scope, brand) {
          self = this;
          //fetch new notifications
            $http({
              method: 'JSONP',
              url: "http://"+self.host+"/appserver.php?callback=JSON_CALLBACK&p&action=updateNotifications",
              params: {"memberid" : localStorage.userid},
            })
            .then(function(response) {
              log(response);
              var data = response.data;

              $rootScope.$notifications = data.length;

              if( data.length == 0 ) $rootScope.$showNotification = false;
              else if( data.length > 0 ) $rootScope.$showNotification = true;
            });

            //StatusBar.overlaysWebView( false );
            scope.$nv = true;
            scope.$needsBack = back;
            scope.$brand = brand;
            scope.$navbartop = true;
            scope.$userid = localStorage.userid;
            //for some reason localStorage.supplier is stored as a string even though i assign a boolean value,
            //so this is how i correct it
            scope.$supplier = (localStorage.supplier == "true") ? true : false;
            scope.$navbarbottom = true;
            scope.$alternateoption = false;
            scope.$supplierProfileNavbar = false;
            scope.$edit_navbar = false;

            $(".navbar-absolute-bottom div").removeClass("active-menu-item");
            switch(brand) {
                case "Kurima":
                    $(".navbar-absolute-bottom #feed").addClass("active-menu-item");
                    break;
                case "Suppliers":
                    $(".navbar-absolute-bottom #suppliers").addClass("active-menu-item");
                    break;
                case "Production":
                    $(".navbar-absolute-bottom #production").addClass("active-menu-item");
                    break;
                case "Collections":
                    $(".navbar-absolute-bottom #collections").addClass("active-menu-item");
                    break;
                case "Notifications":
                    $(".navbar-absolute-bottom #notifications").addClass("active-menu-item");
                    break;
                case "Edit Profile":
                    $(".navbar-absolute-bottom #profile").addClass("active-menu-item");
                    break;
                case "View Supplier":
                    scope.$navbarbottom = false;
                    scope.$supplierProfileNavbar = true;
                    break;
            }
        }
    }
})
;
