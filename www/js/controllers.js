angular.module('kurima.controllers', [])

.controller('notificationsControl', function($scope, $location, global, $rootScope, notificationsService) {

    global.updateNavBar(true, $rootScope, "Notifications");

    //delete any old notifications
    $scope.notifications = undefined;

    $scope.reloadNotifications = function() {
      notificationsService.getNotifications($scope);
    }

    $scope.gotoview = function(id) {
        localStorage.postid = id;
        $location.path("/viewPost");
    }

    notificationsService.getNotifications($scope);

})

.controller('productionControl', function($scope, $location, global, $rootScope, supplierService, feedService) {

    global.updateNavBar(false, $rootScope, "Production");

})

.controller('addproductControl', function($scope, $location, global, $rootScope, supplierService, feedService) {

    global.updateNavBar(true, $rootScope, "Product / Service");
    feedService.getCategories($scope);
    $scope.supplierid = localStorage.userid;

    $scope.updateProduct = function() {
      supplierService.updateProduct($scope, localStorage.viewproduct);
    }

})

.controller('productsControl', function($scope, $location, global, $rootScope, $routeParams, supplierService) {

    global.updateNavBar(true, $rootScope, "Products & Services");

    supplierService.getProducts($scope, $routeParams.search);

    $scope.reloadProducts = function() {
      supplierService.getProducts($scope);
    }

    $scope.viewproduct = function(id) {
      localStorage.viewproduct = id;
      $location.path("/viewproduct");
    }

})

.controller('profileControl', function($scope, $location, global, $rootScope, feedService, $routeParams, supplierService) {

    global.updateNavBar(true, $rootScope, "Edit Profile");

    if( typeof $routeParams.id != "undefined" ) {
        $rootScope.$nv = false;
        $scope.firstime = true;
        $scope.interests = "0";

        $scope.farmer= function() {
            if( $("#interests").val() == 0 ) {
                $scope.$isnotfarmer = true;
                $scope.$isfarmer = false;
            }
            else if( $("#interests").val() == 1 ) {
                $scope.$isfarmer = true;
                $scope.$isnotfarmer = false;
            }
        }

    }

    if( localStorage.supplier ) $rootScope.$edit_navbar = true;

    supplierService.getProfile($scope, localStorage.userid);

    feedService.getLand($scope);
    feedService.getDistricts($scope);
    feedService.getCategories($scope);
    feedService.getCrops($scope);
    feedService.getLivestock($scope);

    $scope.updateProfile = function(b) {
      supplierService.updateProfile($scope, b);
    }

    $scope.changeAvator = function() {
        feedService.getImage("avator", $scope);
    }

    $scope.changeCover = function() {
        feedService.getImage("cover", $scope);
    }

})

.controller('viewproductControl', function($scope, $location, global, $rootScope, supplierService) {

    global.updateNavBar(true, $rootScope, "Product / Service");
    supplierService.getProduct($scope, localStorage.viewproduct);

    $scope.reloadProduct = function() {
      supplierService.getProduct($scope, localStorage.viewproduct);
    }

    $scope.gotosupplier = function(id) {
        localStorage.viewsupplier = id;
        $location.path("/supplier");
    }

    $rootScope.$follow = function(id, bool) {
      supplierService.follow($rootScope, id, bool);
    }

})

.controller('collectionsControl', function($scope, $location, appService, global, $rootScope) {

    global.updateNavBar(false, $rootScope, "Collections");

    $scope.reloadCollections = function() {
        appService.getCollections($scope);
    }

    appService.getCollections($scope);

})

.controller('supplierControl', function($scope, $location, supplierService, global, $rootScope) {

    global.updateNavBar(true, $rootScope, "View Supplier");

    $scope.reloadSupplier = function() {
        supplierService.getProfile($scope, localStorage.viewsupplier);
    }

    $scope.viewproduct = function(id) {
      localStorage.viewproduct = id;
      $location.path("/viewproduct");
    }

    $rootScope.$follow = function(id, bool) {
      supplierService.follow($rootScope, id, bool);
    }

    supplierService.getProfile($scope, localStorage.viewsupplier);

})

.controller('catControl', function($scope, $location, appService, global, $rootScope, filterFilter) {

    global.updateNavBar(true, $rootScope, "Categories");

    $scope.reloadCategories = function() {
      appService.getCategories($scope);
    }

    appService.getCategories($scope);

})

.controller('suppliersControl', function($scope, $location, appService, global, $rootScope, $routeParams, supplierService) {

    $scope.reloadSuppliers = function() {
        appService.getSuppliers($scope);
    }

    $scope.gotosupplier = function(id) {
        localStorage.viewsupplier = id;
        $location.path("/supplier");
    }

    $rootScope.$follow = function(id, bool, i) {
      supplierService.follow($rootScope, id, bool, $scope, i);
    }

    //if there are routeParams then change a few things on this page
    if( typeof $routeParams.search != 'undefined' ) global.updateNavBar(true, $rootScope, "Suppliers");
    else global.updateNavBar(false, $rootScope, "Suppliers");

    appService.getSuppliers($scope, $routeParams.search, $routeParams.value);

})

.controller('viewControl', function($scope, $location, feedService, $rootScope, global, $rootScope) {

    global.updateNavBar(true, $rootScope, "View Post");

    $scope.getPost = function() {
        feedService.getPost( $scope, localStorage.postid );
    }

    $scope.submitComment = function() {
        $rootScope.vscope = $scope;
        feedService.placePost($scope, false, $rootScope.vscope, feedService);
    }

    $scope.placeFav = function() {
        feedService.placeFav($scope, "false");
    }

    $scope.removeFav = function() {
        feedService.placeFav($scope, "true");
    }

    $scope.placeincollection = function() {
        feedService.collections($scope, "false", localStorage.postid );
    }

    $scope.removefromcollection = function() {
        feedService.collections($scope, "true", localStorage.postid );
    }

    $scope.gotosupplier = function(id) {
        localStorage.viewsupplier = id;
        $location.path("/supplier");
    }

    feedService.getPost( $scope, localStorage.postid );

})

.controller('postControl', function($scope, $location, loginService, feedService, global, $rootScope, $routeParams) {

  global.updateNavBar(true, $rootScope, "Publish Post");

  feedService.getCategories($scope);

  $scope.submitMainPost = function() {
      $rootScope.fscope = $scope;
      feedService.placePost($scope, true, $rootScope.fscope, feedService);
  }

  $scope.getImage = function() {
      feedService.getImage("post");
  }

})

.controller('feedControl', function($scope, $location, loginService, feedService, global, $rootScope, $routeParams) {

    if( !loginService.checkLogin() ) location.reload();

    $scope.reloadFeed = function() {
        feedService.getFeed($scope);
    }

    $scope.uploadImage = function(imageuri, postid, bool) {
        feedService.uploadPhoto(imageuri, postid, bool);
    }

    $scope.gotoview = function(id) {
        localStorage.postid = id;
        $location.path("/viewPost");
    }

    //if there are routeParams then change a few things on the feed page
    if( typeof $routeParams.search != 'undefined' ){
      global.updateNavBar(true, $rootScope, "Posts");
      $scope.disableposting = true;
    }else
      global.updateNavBar(false, $rootScope, "Kurima");


    try { StatusBar.overlaysWebView( false ); }
    catch (e) { log(e); }
    feedService.getFeed($scope, $routeParams.search, $routeParams.value);
    //feedService.requestCategories();

})

.controller('gettingStartedControl', function($scope, $location, $rootScope, appService, $routeParams, supplierService) {
    $rootScope.$nv = false;

    log(localStorage.userid);

    $scope.userid = localStorage.userid;

    appService.getCategories($scope);

    $rootScope.$follow = function(id, bool, i, i2) {
    supplierService.follow($rootScope, id, bool, $scope, i, i2);
    }

})

.controller('landingControl', function($scope, $location, $rootScope, loginService, feedService) {
    $rootScope.$nv = false;

    setTimeout(function() {
        $scope.$apply(function() {
            $location.path("/signin");
        })
    }, 3000);
    //StatusBar.overlaysWebView( true );
})

.controller('signinControl', function($scope, $location, $rootScope, loginService, feedService) {

  if( loginService.checkLogin() ) {
        $location.path("/feed");
  }

  $rootScope.$nv = false;
  $scope.issupplier = $scope.found = false;

  $scope.supplier= function() {
    if( $("#type").val() == 0 ) $scope.issupplier = false;
    else if( $("#type").val() == 1 ) $scope.issupplier = true;
  }

  $scope.check = function() {
    loginService.check($scope.email, $scope);
  }

  $scope.attemptLogin = function() {
    loginService.login($scope.email, $scope.password);
  }

})

.controller('signupControl', function($scope, $location, $rootScope, loginService, feedService) {
    $rootScope.$nv = false;

    feedService.getCategories($scope);
    feedService.getDistricts($scope);
    $scope.type = $scope.category = $scope.district = "0";

    $scope.supplier= function() {
      if( $("#type").val() == 0 ) $scope.issupplier = false;
      else if( $("#type").val() == 1 ) $scope.issupplier = true;
    }

    $scope.attemptSignup = function() {
        loginService.signUp();
    }

    //log( loginService.checkLogin() );

})

.controller('menuEvents', function($scope, $rootScope, loginService, feedService, $location) {

    $scope.reloadFeed = function() {
        $location.path("/feed");
    }

    $scope.gotoCategories = function() {
        $location.path("/categories");
    }

})

.controller('kurimaApp', function($scope, loginService, notificationsService) {
    $scope.logout = function() {
        loginService.logout($scope);
    };

    $scope.$back = function() {
        window.history.back();
    }

    //notificationsService.getNotifications($scope);

})
